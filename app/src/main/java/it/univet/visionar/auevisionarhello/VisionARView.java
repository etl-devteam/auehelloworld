package it.univet.visionar.auevisionarhello;

import android.app.Activity;
import android.content.Context;
import android.content.ContextWrapper;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.widget.FrameLayout;

import it.univet.visionar.libvarp.*;

public class VisionARView extends FrameLayout {

    static private String TAG = "AUEHELVW";

    private MainActivity myActivity;

    public VisionARView(Context context, AttributeSet attrs)  {
        super(context, attrs);

        myActivity = null;
        while (context instanceof ContextWrapper) {
            if (context instanceof Activity) {
                myActivity = (MainActivity) context;
            }
            context = ((ContextWrapper)context).getBaseContext();
        }
    }

    @Override
    public void dispatchDraw(Canvas canvas) {
        VisionARLog.i(TAG, new Throwable().getStackTrace()[0].getMethodName());

        super.dispatchDraw(canvas);

        VisionARCanvas myCanvas = new VisionARCanvas();

        Paint paint = new Paint();
        paint.setColor(Color.YELLOW);
        paint.setStyle(Paint.Style.FILL);
        myCanvas.drawPaint(paint);

        paint.setColor(Color.RED);
        switch (myActivity.getSelector()) {
            case 1:
                paint.setStrokeWidth(2.0f);
                paint.setTextSize(40.0f);
                myCanvas.drawText("ACCELEROMETER" , 20, 40, paint);
                paint.setStrokeWidth(1.0f);
                paint.setTextSize(20.0f);
                myCanvas.drawText("X = " + myActivity.getAccelerometerX(), 20, 70, paint);
                myCanvas.drawText("Y = " + myActivity.getAccelerometerY(), 20, 100, paint);
                myCanvas.drawText("Z = " + myActivity.getAccelerometerZ(), 20, 130, paint);
                break;
            case 2:
                paint.setStrokeWidth(2.0f);
                paint.setTextSize(40.0f);
                myCanvas.drawText("GYROSCOPE" , 20, 40, paint);
                paint.setStrokeWidth(1.0f);
                paint.setTextSize(20.0f);
                myCanvas.drawText("X = " + myActivity.getGyroscopeX(), 20, 70, paint);
                myCanvas.drawText("Y = " + myActivity.getGyroscopeY(), 20, 100, paint);
                myCanvas.drawText("Z = " + myActivity.getGyroscopeZ(), 20, 130, paint);
                break;
            case 3:
                paint.setStrokeWidth(2.0f);
                paint.setTextSize(40.0f);
                myCanvas.drawText("MAGNETOMETER" , 20, 40, paint);
                paint.setStrokeWidth(1.0f);
                paint.setTextSize(20.0f);
                myCanvas.drawText("X = " + myActivity.getMagnetometerX(), 20, 70, paint);
                myCanvas.drawText("Y = " + myActivity.getMagnetometerY(), 20, 100, paint);
                myCanvas.drawText("Z = " + myActivity.getMagnetometerZ(), 20, 130, paint);
                break;
            default:
                break;
        }

        myCanvas.draw();
    }
}