package it.univet.visionar.auevisionarhello;

import android.os.Bundle;
import android.os.Handler;

import it.univet.visionar.libvarp.*;

public class MainActivity extends VisionARActivity {

    private static final String TAG = "AUEHELAC";

    private class VisionARClientTaskMagnetometer extends VisionARClientTask {
        @Override
        protected void onPostExecute(VisionARInputMessage result) {
            VisionARLog.v(TAG, "Get Magnetometer Response:" + result.getResponse());

            VisionARInputGetMagnetometerResponseMessage response = new VisionARInputGetMagnetometerResponseMessage(result);
            if (response.getResponse() == VisionARMessage.OK_RESPONSE) {
                magnetometerX = response.getMagnetometerX();
                magnetometerY = response.getMagnetometerY();
                magnetometerZ = response.getMagnetometerZ();
                VisionARLog.v(TAG, "* mag X: " + magnetometerX);
                VisionARLog.v(TAG, "* mag Y: " + magnetometerY);
                VisionARLog.v(TAG, "* mag Z: " + magnetometerZ);
            }

            findViewById(R.id.main_view).invalidate();
        }
    }

    private class VisionARClientTaskAccelerometer extends VisionARClientTask {
        @Override
        protected void onPostExecute(VisionARInputMessage result) {
            VisionARLog.v(TAG, "Get Accelerometer Response:" + result.getResponse());

            VisionARInputGetAccelerometerResponseMessage response = new VisionARInputGetAccelerometerResponseMessage(result);
            if (response.getResponse() == VisionARMessage.OK_RESPONSE) {
                accelerometerX = response.getAccelerometerX();
                accelerometerY = response.getAccelerometerY();
                accelerometerZ = response.getAccelerometerZ();
                VisionARLog.v(TAG, "* acc X: " + accelerometerX);
                VisionARLog.v(TAG, "* acc Y: " + accelerometerY);
                VisionARLog.v(TAG, "* acc Z: " + accelerometerZ);
            }

            findViewById(R.id.main_view).invalidate();
        }
    }

    private class VisionARClientTaskGyroscope extends VisionARClientTask {
        @Override
        protected void onPostExecute(VisionARInputMessage result) {
            VisionARLog.v(TAG, "Get Gyroscope Response:" + result.getResponse());

            VisionARInputGetGyroscopeResponseMessage response = new VisionARInputGetGyroscopeResponseMessage(result);
            if (response.getResponse() == VisionARMessage.OK_RESPONSE) {
                gyroscopeX = response.getGyroscopeX();
                gyroscopeY = response.getGyroscopeY();
                gyroscopeZ = response.getGyroscopeZ();
                VisionARLog.v(TAG, "* gyr X: " + gyroscopeX);
                VisionARLog.v(TAG, "* gyr Y: " + gyroscopeY);
                VisionARLog.v(TAG, "* gyr Z: " + gyroscopeZ);
            }

            findViewById(R.id.main_view).invalidate();
        }
    }

    private Handler mMainHandler = new Handler();

    private int mInterval = 5000;   // reading polling period
    private int mSelector = 1;      // 1. Show Accelerometer inf
                                    // 2. Show Gyroscope info
                                    // 3. Show Magnetometer info

    private float accelerometerX = 0.0f;
    private float accelerometerY = 0.0f;
    private float accelerometerZ = 0.0f;

    private float gyroscopeX = 0.0f;
    private float gyroscopeY = 0.0f;
    private float gyroscopeZ = 0.0f;

    private float magnetometerX = 0.0f;
    private float magnetometerY = 0.0f;
    private float magnetometerZ = 0.0f;

    Runnable mStatusChecker = new Runnable() {
        @Override
        public void run() {
            try {
                VisionARLog.i(TAG);

                switch (getSelector()) {
                    case 1:
                        updateAccelerometerInfo(); //this function can change value of mInterval.
                        break;
                    case 2:
                        updateGyroscopeInfo(); //this function can change value of mInterval.
                        break;
                    case 3:
                    default:
                        updateMagnetometerInfo(); //this function can change value of mInterval.
                        break;
                }
            } finally {
                VisionARLog.i(TAG, "postDelayed!");
                // 100% guarantee that this always happens, even if
                // your update method throws an exception
                mMainHandler.postDelayed(mStatusChecker, mInterval);
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        vibration(100);
        vibration(100);

        startRepeatingTask();
    }

    void startRepeatingTask() {
        mStatusChecker.run();
    }

    void stopRepeatingTask() {
        mMainHandler.removeCallbacks(mStatusChecker);
    }

    @Override
    public void onDestroy() {
        stopRepeatingTask();
        super.onDestroy();
    }

    @Override
    protected void onControlUnitKeyReceived(VisionARInputKeyNotification msg) {
        VisionARLog.i(TAG, "code = " + msg.getCode() + ", value = " + msg.getValue());

        if (msg.getValue() == 1) {
            switch (msg.getCode()) {
                case VisionARMessage.DOWN_BUTTON_KEY:
                    mSelector -= 1;
                    if (mSelector < 1) {
                        mSelector = 3;
                    }
                    findViewById(R.id.main_view).invalidate();
                    break;
                case VisionARMessage.UP_BUTTON_KEY:
                    mSelector += 1;
                    if (mSelector > 3) {
                        mSelector = 1;
                    }
                   findViewById(R.id.main_view).invalidate();
                    break;
                default:
                    break;
            }

            VisionARLog.i(TAG, "selector: " + getSelector());
        }
    }

    private void vibration(int ms) {
        VisionARLog.i(TAG, new Throwable().getStackTrace()[0].getMethodName() + "(" + ms + ")");

        try {
            VisionAROutputSetHapticMessage message = new VisionAROutputSetHapticMessage(ms);
            VisionARClientTask task = new VisionARClientTask();
            task.execute( new VisionARClientTaskParams(message));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void updateMagnetometerInfo() {
        VisionARLog.i(TAG, new Throwable().getStackTrace()[0].getMethodName());

        try {
            VisionAROutputGetRequestMessage message = new VisionAROutputGetRequestMessage(VisionARMessage.GET_MAG_DATA);
            VisionARClientTaskMagnetometer task = new VisionARClientTaskMagnetometer();
            task.execute( new VisionARClientTaskParams(message));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void updateAccelerometerInfo() {
        VisionARLog.i(TAG, new Throwable().getStackTrace()[0].getMethodName());

        try {
            VisionAROutputGetRequestMessage message = new VisionAROutputGetRequestMessage(VisionARMessage.GET_ACC_DATA);
            VisionARClientTaskAccelerometer task = new VisionARClientTaskAccelerometer();
            task.execute( new VisionARClientTaskParams(message));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void updateGyroscopeInfo() {
        VisionARLog.i(TAG, new Throwable().getStackTrace()[0].getMethodName());

        try {
            VisionAROutputGetRequestMessage message = new VisionAROutputGetRequestMessage(VisionARMessage.GET_GYR_DATA);
            VisionARClientTaskGyroscope task = new VisionARClientTaskGyroscope();
            task.execute( new VisionARClientTaskParams(message));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public int getSelector() { return mSelector; }

    public float getMagnetometerX() { return magnetometerX; }
    public float getMagnetometerY() { return magnetometerY; }
    public float getMagnetometerZ() { return magnetometerZ; }

    public float getAccelerometerX() { return accelerometerX; }
    public float getAccelerometerY() { return accelerometerY; }
    public float getAccelerometerZ() { return accelerometerZ; }

    public float getGyroscopeX() { return gyroscopeX; }
    public float getGyroscopeY() { return gyroscopeY; }
    public float getGyroscopeZ() { return gyroscopeZ; }
}